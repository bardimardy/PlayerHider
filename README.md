# MakePluginsGreatAgain

## Intention

Dieses Projekt, soll als für alle zugängliche Plattform zur Verbesserung verschiedenster entwickelten Server-Plugins (tendenziell mit deutlichen Verbesserungsmöglichkeiten) dienen.
Wir möchten hierbei die Möglichkeit eröffnen gemeinsam Ansätze zu immer wieder auftretenden Problemen und Programmier-Paradigmen zu diskutieren und den produktiven Austausch zu solchen fördern.

Den Start macht das `PlayerHider SpeedCode Plugin` von WeLoveSpigotPlugins.

## Projekte

### PlayerHider SpeedCode

Vorgenommene Änderungen:

*  Data.class - Ein paar sinnvolle Veränderungen
*  Main.class - Konsolenausgabe über den Bukkit-Logger
*  PlayerHiderListener.class - CopyOnWriteArrayList's entfernt und eine ArrayList genutzt da diese sinnvoller ist
*  PlayerHiderListener.class - Anstatt von einem Scheduler je Spieler für die Cooldowns eine HashMap genutzt mit jeweils zugeordneten Timestamps
*  PlayerHiderListener.class - "Hide" Logik in drei einzelne Methoden verschachtelt

## Entwickler

Original von WeLoveSpigotPlugins.
Bearbeitet von Kameramann aka. Klopapier.

In diesem Sinne:
#makeWeLoveSpigotPluginsGreatAgain